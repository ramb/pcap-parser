#pragma once

#include <iostream>
#include <vector>

#include <boost/serialization/type_info_implementation.hpp>
#include <boost/archive/basic_archive.hpp>
#include <boost/serialization/serialization.hpp>

#include <boost/serialization/nvp.hpp>
#include <boost/serialization/version.hpp>
#include <boost/serialization/vector.hpp>

namespace boost {
namespace serialization {
    template <typename Ar>
    void serialize(Ar& ar, timeval& tv, const unsigned version) {
        ar & boost::serialization::make_nvp("tv_sec", tv.tv_sec);
        ar & boost::serialization::make_nvp("tv_usec", tv.tv_usec);
    }
}
}

namespace DPI
{
    enum class Direction : int {
        Forward = 1,
        Reverse
    };

    struct Flow {
        uint64_t id{};

        uint32_t src_ip{}, dst_ip{};
        uint16_t src_port{}, dst_port{};

        std::string master_protocol, app_protocol, category;

        bool bidirectional{};

        template<typename Ar> void serialize(Ar& ar, const unsigned version) {
            ar & BOOST_SERIALIZATION_NVP(id);
            ar & BOOST_SERIALIZATION_NVP(src_ip);
            ar & BOOST_SERIALIZATION_NVP(dst_ip);
            ar & BOOST_SERIALIZATION_NVP(src_port);
            ar & BOOST_SERIALIZATION_NVP(dst_port);
            ar & BOOST_SERIALIZATION_NVP(master_protocol);
            ar & BOOST_SERIALIZATION_NVP(app_protocol);
            ar & BOOST_SERIALIZATION_NVP(category);
            ar & BOOST_SERIALIZATION_NVP(bidirectional);
        }
    };

    struct Packet {
        uint64_t flow_id{};

        Direction direction = Direction::Forward;
        timeval time_stamp{};
        uint32_t frame_length{};

        template<typename Ar> void serialize(Ar& ar, const unsigned version) {
            ar & BOOST_SERIALIZATION_NVP(flow_id);
            ar & BOOST_SERIALIZATION_NVP(direction);
            ar & BOOST_SERIALIZATION_NVP(time_stamp);
            ar & BOOST_SERIALIZATION_NVP(frame_length);
        }
    };

    struct Dump {
        std::vector<Packet> packets;
        std::vector<Flow> flows;

        bool validate() const;

        template<typename Ar> void serialize(Ar& ar, const unsigned version) {
            ar & BOOST_SERIALIZATION_NVP(packets);
            ar & BOOST_SERIALIZATION_NVP(flows);
        }
    };
}
