#pragma once

#include "typedefs.h"

#include <boost/filesystem.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>

#include <boost/archive/polymorphic_xml_iarchive.hpp>
#include <boost/archive/polymorphic_xml_oarchive.hpp>

namespace DPI
{
    bool load(const std::wstring& path, Dump& dump);
    bool dump(const std::wstring& path, const Dump& dump);
    bool dump_csv(const std::string &path, const Dump &dump);
}