#include "typedefs.h"

#include <algorithm>

bool DPI::Dump::validate() const {
    if (packets.empty() || flows.empty())
        return false;

    for (const auto& packet : packets) {
        auto it = std::find_if(flows.cbegin(), flows.cend(), [&packet](const Flow& flow) {
            return flow.id == packet.flow_id;
        });

        if (it == flows.cend())
            return false;
    }

    return true;
}