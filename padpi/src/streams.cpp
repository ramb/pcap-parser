#include "streams.h"

#include <pcapplusplus/IpAddress.h>

#include "persist/csvwriter.h"

#include <sys/time.h>
#include <boost/format.hpp>
#include <sstream>

namespace
{

std::string make_datetime(const timeval& tv)
{
    time_t t = tv.tv_sec;
    std::tm* tm = localtime(&t);

//    return (boost::format("%1%/%2%/%3% %4%:%5%:%6%")
//        % tm->tm_mon % tm->tm_mday % tm->tm_year
//        % tm->tm_hour % tm->tm_min % tm->tm_sec
//    ).str();

    std::stringstream ss;
    ss  << tm->tm_mon << "/" << tm->tm_mday << "/" << (1900 + tm->tm_year)
        << " " << tm->tm_hour << ":" << tm->tm_min << ":" << tm->tm_sec;

    return ss.str();
}

}

bool DPI::dump(const std::wstring &path, const DPI::Dump &dump) {
//    if (!dump.validate())
//        return false;

    boost::filesystem::ofstream file(path);
    boost::archive::binary_oarchive oa(file);
    oa << dump;

    return true;
}

bool DPI::load(const std::wstring &path, DPI::Dump &dump) {
    boost::filesystem::ifstream file(path);
    boost::archive::binary_iarchive ia(file);

    ia >> dump;

//    return dump.validate();
    return true;
}

bool DPI::dump_csv(const std::string &path, const DPI::Dump &dump) {
    CSVWriter csvWriter;

    csvWriter.newRow()
        << "Flow ID" << "Src IP" << "Src Port" << "Dst IP" << "Dst Port"
        << "Master protocol" << "App protocol" << "Category" << "Bidirectional"
        << "Direction" << "Timestamp" << "Frame length";


    std::vector<DPI::Flow>::const_iterator it, last_it = dump.flows.cend();
    size_t num_of_packets = dump.packets.size();
    unsigned done_percent = 0;
    std::cout << std::to_string(done_percent) << "%   ";
    std::cout.flush();

    for (size_t i = 0; i < dump.packets.size(); i++) {

        auto percent = static_cast<unsigned>((static_cast<double>(i) / num_of_packets) * 100.);

        if (percent > done_percent) {
            done_percent = percent;
            std::cout << "\r" << std::to_string(done_percent) << "%   ";
            std::cout.flush();
        }


        const auto& packet = dump.packets.at(i);

        if (last_it != dump.flows.cend() && (*last_it).id == packet.flow_id)
            it = last_it;
        else {
            it = std::find_if(dump.flows.cbegin(), dump.flows.cend(),
                    [&packet](const DPI::Flow& f){ return f.id == packet.flow_id; });
            last_it = it;
        }

        if (it == dump.flows.cend())
            continue;

        const DPI::Flow& flow = *it;

        if (flow.id != packet.flow_id)
            continue;

        pcpp::IPv4Address srcIp{flow.src_ip}, dstIp{flow.dst_ip};

        csvWriter.newRow()
            << flow.id << srcIp.toString() << flow.src_port << dstIp.toString() << flow.dst_port
            << flow.master_protocol << flow.app_protocol << flow.category << flow.bidirectional
            << (packet.direction == DPI::Direction::Forward ? "Forward" : "Reverse")
            << make_datetime(packet.time_stamp) << packet.frame_length;
    }

    std::cout << "\r" <<  "100% Writing to disk..." << std::endl;

    csvWriter.writeToFile(path);

    return true;
}