#pragma once

#include "persist/stdafx.h"

namespace core {

bool check_network_permissions();

void init_dumps_folder();

void convert(const std::string& input, const std::string& output);

};

