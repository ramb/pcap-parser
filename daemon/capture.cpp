#include "capture.h"

#include "core.h"
#include "detection.h"

#include "padpi/streams.h"

#include <boost/format.hpp>

Capture::Capture(ApplicationStatePtr state, const std::string& interface_name)
    : state_(state)
{
    LOG << "Trying to create capture device for " << interface_name << " interface";
    dev_ = pcpp::PcapLiveDeviceList::getInstance().getPcapLiveDeviceByName(interface_name);
    if (dev_ == nullptr) {
        LOG << "Failed to create capture device";
        throw std::runtime_error("Failed to create capture device");
    }
    LOG << "Capture device for interface " << interface_name << " created successfully";
    LOG << "IP: "           << dev_->getIPv4Address().toString()
        << ", MAC: "        << dev_->getMacAddress().toString()
        << ", Gateway: "    << dev_->getDefaultGateway().toString()
        << ", MTU: " << dev_->getMtu()
        << ( !dev_->getDnsServers().empty() ? ( ", DNS Server: " + dev_->getDnsServers().at(0).toString()) : "");


    LOG << "Trying to initialize nDPI...";
    LOG << "nDPI version: " << std::to_string(ndpi_get_api_version());

    ndpi_detection_module_struct *ptr = ndpi_init_detection_module();
    detection_module_.reset(ptr);

    if (detection_module_ == nullptr) {
        LOG << "Failed to initialize nDPI";
        return;
    }

    ndpi_set_detection_preferences(detection_module_.get(), ndpi_pref_http_dont_dissect_response, 0);
    ndpi_set_detection_preferences(detection_module_.get(), ndpi_pref_dns_dont_dissect_response, 0);
    ndpi_set_detection_preferences(detection_module_.get(), ndpi_pref_enable_category_substring_match, 1);

    LOG << "nDPI detection module initialized successfully";

    NDPI_PROTOCOL_BITMASK all;
    NDPI_BITMASK_SET_ALL(all);

    ndpi_set_protocol_detection_bitmask2(detection_module_.get(), &all);

    workflow_ = std::make_shared<workflow_t>();
}

Capture::~Capture() {
    if (writer_ && writer_->isOpened()) {
        writer_->close();
        LOG << "File for dumping traffic successfully closed";
    }
}

void Capture::startCapture() {
    if (!core::check_network_permissions()) {
        LOG << "Operation not permitted. You should have privileges to open device " << dev_->getName();
        throw std::runtime_error("Operation not permitted. You should have privileges to open device");
    }

    pcpp::PcapLiveDevice::DeviceConfiguration cfg;
    cfg.packetBufferTimeoutMs = 1;

    if (!dev_ || !dev_->open(cfg)) {
        LOG << "Could not open " << dev_->getName() << " device. Does user has the needed permissions?";
        throw std::runtime_error("Could not open device. Does user has the needed permissions?");
    }

    if (!dev_->isOpened()) {
        LOG << "Device is not opened before calling Capture::startCapture";
        throw std::logic_error("Device should be opened before calling Capture::startCapture");
    }

    LOG << "Device " << dev_->getName() << " successfully opened";

    storage_item_ = state_->createItem();

    writer_ = std::make_unique<pcpp::PcapNgFileWriterDevice>(storage_item_->path);
    if (!writer_->open()) {
        LOG << "Could not open file for dumping traffic";
        throw std::runtime_error("Could not open file");
    }

    LOG << "File for dumping traffic created successfully";

    cookie_ = std::make_shared<cookie_t>(writer_, detection_module_, workflow_);

    auto res = dev_->startCapture(&Capture::onPacketArrives, cookie_.get());
    LOG << "Capture started with res " << res << " for device " << dev_->getName();

    active_capture_ = true;
}

void Capture::stopCapture() {
    if (!active_capture_) {
        LOG << "Trying to stop not active capture";
        return;
    }

    dev_->stopCapture();

    LOG << "Capture stopped";

    if (dev_ && dev_->isOpened()) {
        const std::string devName = dev_->getName();
        LOG << "Trying to close " << devName << " device";
        dev_->close();
        LOG << "Device " << devName << " successfully closed";
    }

    if (writer_ && writer_->isOpened()) {
        LOG << "Trying to close writer with name " << writer_->getFileName();
        writer_->close();
        LOG << "Writer with name " << writer_->getFileName() << " was successfully close";
    }

    storage_item_->finished = true;
    storage_item_ = nullptr;

    active_capture_ = false;

    auto t = std::time(0);
    std::tm* now = std::localtime(&t);

    std::wstring bin_file_path = (boost::wformat(L"dump_%1%-%2%-%3%_%4%-%5%.bin")
        % (now->tm_year + 1900) % (now->tm_mon + 1) % now->tm_mday
        % now->tm_hour % now->tm_min).str();

    std::string csv_file_path = (boost::format("dump_%1%-%2%-%3%_%4%-%5%.csv")
        % (now->tm_year + 1900) % (now->tm_mon + 1) % now->tm_mday
        % now->tm_hour % now->tm_min).str();

    if (DPI::dump(bin_file_path, workflow_->dump)) {
        std::wcout << L"Dump saved correctly" << std::endl;
    } else {
        std::wcout << L"Error when saving dump file to dist" << std::endl;
    }

    if (DPI::dump_csv(csv_file_path, workflow_->dump))
        std::wcout << L"CSV dump saved correctly" << std::endl;
}

void Capture::onPacketArrives(pcpp::RawPacket* packet, pcpp::PcapLiveDevice* dev, void* c) {
    auto cookie = static_cast<cookie_t*>(c);
    auto writer = cookie->writer;

    {
        std::lock_guard<std::mutex> lock(capture_mutex);
        writer->writePacket(*packet);
    }

    process_detection(packet, cookie);
}
