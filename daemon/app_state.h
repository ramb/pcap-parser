#pragma once

#include "persist/stdafx.h"
#include "persist/file_path.h"

#include <uuid.h>

#include "config.h"

struct StorageItem {
    uuids::uuid id;
    std::wstring name;
    file_path path;
    std::chrono::system_clock::time_point creation_time;
    bool finished{};

    StorageItem() = default;

    StorageItem(const uuids::uuid& id, const std::wstring& name, const file_path& path)
        : id(id)
        , name(name)
        , path(path)
    {
        creation_time = std::chrono::system_clock::now();
    }

    template <typename Archive>
    void serialize(Archive& ar, const unsigned version) {
        ar & BOOST_SERIALIZATION_NVP(id);
        ar & BOOST_SERIALIZATION_NVP(name);
        ar & BOOST_SERIALIZATION_NVP(path);
        ar & BOOST_SERIALIZATION_NVP(creation_time);
        ar & BOOST_SERIALIZATION_NVP(finished);
    }
};

using StorageItemPtr = std::shared_ptr<StorageItem>;

class ApplicationState
{
    friend class Application;
    friend class boost::serialization::access;

    template<class Archive>
    void serialize(Archive& ar, const unsigned version) {
        ar & BOOST_SERIALIZATION_NVP(storage_);
    }

public:
    ApplicationState() = default;
    StorageItemPtr createItem();
    const std::vector<StorageItemPtr>& allItems() const;
private:
    std::vector<StorageItemPtr> storage_;
    uuids::uuid_system_generator uuid_gen_;
};

using ApplicationStatePtr = std::shared_ptr<ApplicationState>;

BOOST_CLASS_VERSION(ApplicationState, 0)
