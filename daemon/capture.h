#pragma once

#include "persist/stdafx.h"

#ifndef NDPI_LIB_COMPILATION
#define NDPI_LIB_COMPILATION
#endif

#include <pcapplusplus/PcapLiveDevice.h>
#include <pcapplusplus/PcapLiveDeviceList.h>
#include <pcapplusplus/PcapFileDevice.h>
#include <pcapplusplus/Packet.h>

#include <ndpi_main.h>

#include "app_state.h"
#include "padpi/typedefs.h"

#define MAX_NDPI_FLOWS         200000
#define MAX_EXTRA_PACKETS_TO_CHECK  7

static std::mutex capture_mutex;

struct ndpi_stats_t {
    uint64_t protocol_counter{};
    uint64_t protocol_counter_bytes{};
    uint64_t protocol_flows{};
};

struct flow_t {
    flow_t(const flow_t&) = delete;
    flow_t(const flow_t&&) = delete;

    uint64_t id{};

    uint32_t src_ip{}, dst_ip{};
    uint16_t src_port{}, dst_port{};
    uint8_t protocol{};
    bool bidirectional{};
    ndpi_protocol detected_protocol;
    uint8_t src_to_dst_direction = 1;

    std::shared_ptr<ndpi_flow_struct> flow;
    std::mutex flow_mutex;
    std::shared_ptr<ndpi_id_struct> src_id;
    std::shared_ptr<ndpi_id_struct> dst_id;

    uint8_t proto{};
    uint64_t src2dst_packets{};
    uint64_t dst2src_packets{};

    bool detection_completed{}, check_extra_packets{};
    bool fully_completed{}, printed{};

    flow_t() {
        flow = std::make_shared<ndpi_flow_struct>();

        src_id = std::make_shared<ndpi_id_struct>();
        dst_id = std::make_shared<ndpi_id_struct>();
    }
};

using flow_t_ptr = std::shared_ptr<flow_t>;

struct workflow_t {
    std::vector<flow_t_ptr> ndpi_flows_root;
    DPI::Dump dump;
    uint64_t last_id{};

    workflow_t() {
        ndpi_flows_root.reserve(MAX_NDPI_FLOWS);
    }
};

struct cookie_t {
    std::shared_ptr<pcpp::PcapNgFileWriterDevice> writer;
    std::shared_ptr<ndpi_detection_module_struct> detection_module;
    std::shared_ptr<workflow_t> workflow;

    cookie_t(
        std::shared_ptr<pcpp::PcapNgFileWriterDevice>& wr,
        std::shared_ptr<ndpi_detection_module_struct>& dm,
        std::shared_ptr<workflow_t>& wf
    ) : writer(wr), detection_module(dm), workflow(wf) {};
};

class Capture {
public:
    Capture() = delete;
    Capture(ApplicationStatePtr state, const std::string& interface_name);
    ~Capture();
    void startCapture();
    void stopCapture();

private:
//    static bool onPacketArrivesBlockingMode(pcpp::RawPacket* packet, pcpp::PcapLiveDevice* dev, void* cookie);
    static void onPacketArrives(pcpp::RawPacket* packet, pcpp::PcapLiveDevice* dev, void* cookie);

private:
    bool active_capture_{};
    ApplicationStatePtr state_ = nullptr;
    StorageItemPtr storage_item_ = nullptr;
    pcpp::PcapLiveDevice* dev_ = nullptr;
    std::shared_ptr<pcpp::PcapNgFileWriterDevice> writer_ = nullptr;
    std::shared_ptr<ndpi_detection_module_struct> detection_module_ = nullptr;
    std::shared_ptr<workflow_t> workflow_ = nullptr;

    std::shared_ptr<cookie_t> cookie_ = nullptr;
};
