#pragma once

#include <Scheduler.h>

#include "persist/stdafx.h"
#include "capture.h"
#include "net.h"
#include "tray.h"
#include "app_state.h"

#define STATE_FILE_NAME "state.xml"

class App {
public:
    ~App();
    App(const App&) = delete;
    App& operator= (const App&) = delete;
private:
    explicit App();
    void loadState();
    void saveState();

public:
    static App& instance() {
        static App app;
        return app;
    }
    static void shutdown(int signal);
    int run();
    void stop();
    inline void startCapture() { capture_->startCapture(); }
    inline void stopCapture() { capture_->stopCapture(); }
    inline auto getState() { return state_.get(); }
private:
    std::unique_ptr<Capture> capture_;
    std::unique_ptr<HttpServer> httpServer_;
    std::unique_ptr<Tray> tray_;
    std::unique_ptr<Bosma::Scheduler> scheduler_;
    ApplicationStatePtr state_;
private:
    std::mutex app_mutex;
    std::condition_variable app_cv;
};