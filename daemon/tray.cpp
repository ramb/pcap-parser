#include "tray.h"

#include "app.h"

void Tray::start()
{
    thread_ = std::thread([&](){
        tray_menu menu[] = {
                { const_cast<char*>(START_CAPTURE_LABEL), 0, 0, [](tray_menu* menu){ App::instance().startCapture(); }, nullptr },
                { const_cast<char*>(STOP_CAPTURE_LABEL), 0, 0, [](tray_menu* menu){ App::instance().stopCapture(); }, nullptr },
                { const_cast<char*>(EXIT_LABEL), 0, 0, [](tray_menu* menu){ App::shutdown(0); }, nullptr },
                { nullptr, 0, 0, nullptr, nullptr }
        };

        tray tray = {
                .icon = const_cast<char*>(ICON_FILE),
                .menu = menu
        };

        tray_init(&tray);

        while (tray_loop(1) == 0);
    });
}

void Tray::stop()
{
    tray_exit();
    if (thread_.joinable())
        thread_.join();
}