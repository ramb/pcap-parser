#pragma once

#include "persist/stdafx.h"

#include <pcapplusplus/Packet.h>

#include "capture.h"

void process_detection(pcpp::RawPacket* packet, const cookie_t* cookie);

std::string get_protocol_name(const uint16_t& proto);

std::string get_category_name(const ndpi_protocol& proto);

void setup_parameters(
        pcpp::Packet &packet, uint8_t& proto,
        uint32_t &src_ip, uint32_t &dst_ip, uint16_t &src_port, uint16_t &dst_port,
        ndpi_iphdr*& iphdr, size_t& data_len
);

using flow_cmp_t = std::function<bool(const flow_t_ptr&)>;

flow_t* get_flow(std::vector<flow_t_ptr>& flows_root, flow_cmp_t& comparator, flow_cmp_t& reverse_comparator);

void free_flow(flow_t& flow);
