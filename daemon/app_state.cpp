#include "app_state.h"

StorageItemPtr ApplicationState::createItem() {
    const auto uuid = uuid_gen_();
    const auto& uuid_str = uuids::to_string(uuid);
    const std::wstring file_name = strings::to_wide(CONFIG("pathForDumps", std::string))
            + strings::to_wide(uuid_str) + L".pcapng";
    file_path path{ file_name };

    auto item = std::make_shared<StorageItem>(uuid, file_name, path);

    storage_.emplace_back(item);
    return item;
}

const std::vector<StorageItemPtr>& ApplicationState::allItems() const {
    return storage_;
}