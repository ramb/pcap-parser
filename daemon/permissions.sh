#!/usr/bin/env bash

sudo setcap cap_net_raw,cap_net_admin=eip \
    /home/ramb/Projects/pcap_extract/cmake-build-debug/daemon/pcap_daemon;
