#include "net.h"

#include "app.h"

void handlers::echoHandler(const Rest::Request& request, Http::ResponseWriter response)
{
    response.send(Http::Code::Ok, "I am alive");
}

void handlers::shutdownHandler(const Rest::Request& request, Http::ResponseWriter response)
{
    response.send(Http::Code::Ok, "Server would be turned off");
    std::thread t([](){
        std::this_thread::sleep_for(std::chrono::seconds(1));
        App::instance().stop();
    });
    t.detach();
}

void handlers::startCapture(const Rest::Request &request, Http::ResponseWriter response)
{
    App::instance().startCapture();
    response.send(Http::Code::Ok, "startCapture");
}

void handlers::stopCapture(const Rest::Request &request, Http::ResponseWriter response)
{
    App::instance().stopCapture();
    response.send(Http::Code::Ok, "stopCapture");
}

void handlers::listDumps(const Rest::Request& request, Http::ResponseWriter response)
{
    const auto& items = App::instance().getState()->allItems();

    nlohmann::json response_json = nlohmann::json::array();

    for (const auto& item : items) {
        if (!item->finished) continue;

        nlohmann::json item_json = nlohmann::json::object({
                {"creation_time", std::chrono::duration_cast<std::chrono::microseconds>(
                        item->creation_time.time_since_epoch()).count()},
                {"id", uuids::to_string(item->id)}
        });

        response_json.push_back(item_json);
    }

    response.send(Http::Code::Ok, response_json.dump());
}

void handlers::downloadDump(const Rest::Request& request, Http::ResponseWriter response) {
    const auto id = uuids::uuid::from_string(
            request.param(":id").as<std::string>());

    const auto& items = App::instance().getState()->allItems();
    auto item = std::find_if(items.cbegin(), items.cend(),
            [&id](const StorageItemPtr& item){ return item->id == id; });

    if (item == items.cend()) {
        response.send(Http::Code::Not_Found);
        return;
    }

    auto header = response.headers().add<ContentDisposition>("attachment; filename=\"dump.pcapng\"");

    Http::serveFile(response, (*item)->path, MimePcapng);
}

void setContentType(const Rest::Request& request, Http::ResponseWriter& response) {
    response.headers().add<ContentType>("text/html; charset=utf-8");
}

Route::Handler registerHandler(const Handler& handler, const std::vector<Decorator>& decorators) {
    return [=](const Rest::Request& request, Http::ResponseWriter response) {
        setContentType(request, response);

        for (const auto& decorator : decorators) { decorator(request, response); }

        handler(request, std::move(response));

        return Route::Result::Ok;
    };
}

void setupHandlers(Router& router)
{
    Routes::Get(router, "/", registerHandler(&handlers::echoHandler, {}));
    Routes::Get(router, "/shutdown", registerHandler(&handlers::shutdownHandler, {}));
    Routes::Get(router, "/start", registerHandler(&handlers::startCapture, {}));
    Routes::Get(router, "/stop", registerHandler(&handlers::stopCapture, {}));
    Routes::Get(router, "/list", registerHandler(&handlers::listDumps, {}));
    Routes::Get(router, "/download/:id", registerHandler(&handlers::downloadDump, {}));
}

void setupHeaders() {
    Http::Header::Registry::instance().registerHeader<ContentDisposition>();
}

HttpServer::HttpServer()
{
    LOG << "Initializing router";

    Router router;
    setupHandlers(router);
    setupHeaders();

    Address addr(Ipv4::any(), 9080);
    endpoint_ = std::make_unique<Http::Endpoint>(addr);

    auto opts = Http::Endpoint::options()
            .threads(hardware_concurrency())
            .flags(Tcp::Options::InstallSignalHandler);
    endpoint_->init(opts);

    endpoint_->setHandler(router.handler());
}

void HttpServer::start()
{
    http_thread_ = std::thread([&]() { endpoint_->serve(); });
}

void HttpServer::stop()
{
    endpoint_->shutdown();
    if (http_thread_.joinable())
        http_thread_.join();
}