#pragma once

#include "persist/json.h"
#include "persist/stdafx.h"

class Config {
public:
    static Config& instance() {
        static Config config_;
        return config_;
    }

    Config(const Config&) = delete;
    Config& operator=(const Config&) = delete;
    template<typename T> inline T get(const std::string& key) {
        if (!json_.count(key)) {
            LOG << "No such key in config.json file: \"" << key << "\"";
            throw std::runtime_error("There is no such key in config file: \"" + key + "\"");
        }
        LOG << "Trying to get key " << key << " from config: " << json_.dump();
        return json_[key].get<T>();
    }

private:
    Config();
    ~Config() = default;

private:
    nlohmann::json json_;
};

#define CONFIG(key, type) Config::instance().get<type>(key)