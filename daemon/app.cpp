#include "app.h"

#include <csignal>
#include <chrono>
#include <thread>

#include <boost/serialization/serialization.hpp>
#include <boost/archive/polymorphic_xml_wiarchive.hpp>
#include <boost/archive/polymorphic_xml_woarchive.hpp>

#include "config.h"
#include "net.h"
#include "core.h"

using namespace std::chrono_literals;

App::App()
{
    std::signal(SIGINT, &App::shutdown);
    state_ = std::make_unique<ApplicationState>();

    if (std::filesystem::exists(STATE_FILE_NAME)) {
        loadState();
    }

    // setting up scheduler with a task for saving state every minute
    scheduler_ = std::make_unique<Bosma::Scheduler>(std::thread::hardware_concurrency());
    scheduler_->every(1min, [&](){ saveState(); });
}

void App::loadState() {
    std::lock_guard<std::mutex> lock(app_mutex);

    std::wifstream is(STATE_FILE_NAME);
    boost::archive::polymorphic_xml_wiarchive ia(is);
    ia >> BOOST_SERIALIZATION_NVP(state_);

    LOG << "State was loaded";
}

void App::saveState() {
    std::lock_guard<std::mutex> lock(app_mutex);

    std::wofstream os(STATE_FILE_NAME);
    boost::archive::polymorphic_xml_woarchive oa(os);
    oa << BOOST_SERIALIZATION_NVP(state_);

    LOG << "State was saved";
}

App::~App() {
    saveState();
}

int App::run() {
    core::init_dumps_folder();

    const auto interface_name = CONFIG("interface", std::string);
    capture_ = std::make_unique<Capture>(state_, interface_name);

    httpServer_ = std::make_unique<HttpServer>();
    httpServer_->start();

    tray_ = std::make_unique<Tray>();
    tray_->start();

    std::unique_lock<std::mutex> lock(app_mutex);
    app_cv.wait(lock);

    tray_->stop();
    httpServer_->stop();

    return 0;
}

void App::shutdown(int signal) {
    std::wcout << L"Shutting down the app instance by signal " << signal << std::endl;
    App::instance().stop();
}

void App::stop() {
    app_cv.notify_one();
}
