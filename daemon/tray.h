#pragma once

#include "persist/stdafx.h"

#define TRAY_APPINDICATOR
#include "persist/tray.h"

static char const* START_CAPTURE_LABEL = "Start capture";
static char const* STOP_CAPTURE_LABEL = "Stop capture";
static char const* EXIT_LABEL = "Exit";
static char const* ICON_FILE = "icon.png";

class Tray
{
public:
    Tray() = default;
    void start();
    void stop();

private:
    std::thread thread_;
};