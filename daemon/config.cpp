#include "config.h"

#include "persist/stdafx.h"

#include <filesystem>

Config::Config() {
    LOG << "Trying to load config.json file";

    if (!std::filesystem::exists("config.json")) {
        LOG << "No config file exists";
        throw std::runtime_error("No config file exists");
    }

    std::ifstream file{ "config.json", std::ios::binary };

    file >> json_;

    LOG << "File config.json loaded completely";
    file.close();
}
