#include "core.h"

#include <sys/capability.h>
#include <sys/types.h>
#include <unistd.h>
#include <cstdio>
#include <filesystem>

#include "config.h"
#include "padpi/streams.h"

bool core::check_network_permissions() {
    pid_t pid;
    cap_t cap;
    cap_flag_value_t cap_flags_value;

    pid = getpid();
    cap = cap_get_pid(pid);
    if (cap == NULL) {
        return false;
    }

    bool has_all_permissions = true;
    cap_get_flag(cap, CAP_NET_ADMIN, CAP_EFFECTIVE, &cap_flags_value);
    if (cap_flags_value == CAP_CLEAR)
        has_all_permissions = false;

    cap_get_flag(cap, CAP_NET_RAW, CAP_EFFECTIVE, &cap_flags_value);
    if (cap_flags_value == CAP_CLEAR)
        has_all_permissions = false;

    cap_free(cap);
    return has_all_permissions;
}

void core::init_dumps_folder() {
    const std::wstring dumps_folder = strings::to_wide(CONFIG("pathForDumps", std::string));
    if (!std::filesystem::exists(dumps_folder)) {
        std::filesystem::create_directory(dumps_folder);
    }
}

void core::convert(const std::string& input, const std::string& output) {

    DPI::Dump dump;

    std::wstring input_path = strings::to_wide(input);

    DPI::load(input_path, dump);

    DPI::dump_csv(output, dump);
}