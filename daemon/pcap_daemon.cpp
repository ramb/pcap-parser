#include <pcapplusplus/PcapLiveDeviceList.h>
#include <pcapplusplus/PcapLiveDevice.h>

#include <boost/program_options.hpp>

#include "persist/stdafx.h"
#include "persist/core.h"

#include "config.h"
#include "app.h"
#include "core.h"

namespace po = boost::program_options;

void onPacketArrives(pcpp::RawPacket* packet, pcpp::PcapLiveDevice* dev, void* cookie) {
    auto writer = static_cast<pcpp::PcapNgFileWriterDevice*>(cookie);
    writer->writePacket(*packet);
}

int main(int argc, char** argv) {
    try {
        po::options_description desc("Allowed options");
        desc.add_options()
                ("help", "produce help message")
                ("interfaces", "print all available devices")
                ("daemon", "starts the daemon")
                ("if", po::value<std::string>(), "input file")
                ("of", po::value<std::string>(), "output file")
                ;

        po::variables_map vm{};
        po::store(po::parse_command_line(argc, argv, desc), vm);
        po::notify(vm);

        if (vm.count("help")) {
            std::cout << desc << std::endl;
            return 0;
        }

        if (vm.count("interfaces")) {
            core::printInterfaces();
            return 0;
        }

        if (vm.count("daemon")) {
            return App::instance().run();
        }

        if (vm.count("if") && vm.count("of")) {
            core::convert(vm["if"].as<std::string>(), vm["of"].as<std::string>());
            return 0;
        }

        std::cout << desc << std::endl;
    }
    catch (const std::exception& e) {
        LOG << "Error: " << e.what();
        std::wcout << L"Error: " << e.what() << std::endl;
        return 1;
    }
    return 0;
}
