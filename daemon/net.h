#pragma once

#include "persist/stdafx.h"

#include <persist/json.h>
#include <pistache/endpoint.h>
#include <pistache/router.h>

using json = nlohmann::json;
using namespace Pistache;
using namespace Pistache::Rest;

static auto MimeJson = Http::Mime::MediaType::fromString("application/json");
static auto MimePcapng = Http::Mime::MediaType::fromString("application/vnd.tcpdump.pcap");

class ContentDisposition : public Http::Header::Header {
public:
    NAME("Content-Disposition")

    ContentDisposition() = default;

    ContentDisposition(const std::string& value)
        : value_(value)
    {}

    void parse(const std::string& data) {
        value_ = data;
    }

    void write(std::ostream& os) const {
        os << value_;
    }

private:
    std::string value_;
};

class ContentType : public Http::Header::Header {
public:
    NAME("Content-Type")

    ContentType() = default;

    ContentType(const std::string& value)
            : value_(value)
    {}

    void parse(const std::string& data) {
        value_ = data;
    }

    void write(std::ostream& os) const {
        os << value_;
    }

private:
    std::string value_;
};

typedef std::function<void(const Rest::Request&, Http::ResponseWriter)> Handler;
typedef std::function<void(const Rest::Request&, Http::ResponseWriter&)> Decorator;

namespace handlers
{
    void echoHandler(const Rest::Request& request, Http::ResponseWriter response);
    void shutdownHandler(const Rest::Request& request, Http::ResponseWriter response);
    void startCapture(const Rest::Request& request, Http::ResponseWriter response);
    void stopCapture(const Rest::Request& request, Http::ResponseWriter response);
    void listDumps(const Rest::Request& request, Http::ResponseWriter response);
    void downloadDump(const Rest::Request& request, Http::ResponseWriter response);
}

void setupHandlers(Router& router);

void setupHeaders();

class HttpServer
{
public:
    HttpServer();
    void start();
    void stop();
private:
    std::unique_ptr<Http::Endpoint> endpoint_;
    std::thread http_thread_;
};