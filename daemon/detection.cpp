#include "detection.h"

#include <pcapplusplus/RawPacket.h>
#include <pcapplusplus/IPv4Layer.h>
#include <pcapplusplus/TcpLayer.h>
#include <pcapplusplus/UdpLayer.h>

static std::map<int, std::string> protocol_name_map = {
        { NDPI_PROTOCOL_UNKNOWN, "UNKNOWN" },
        { NDPI_PROTOCOL_FTP_CONTROL, "FTP_CONTROL" },
        { NDPI_PROTOCOL_MAIL_POP, "MAIL_POP" },
        { NDPI_PROTOCOL_MAIL_SMTP, "MAIL_SMTP" },
        { NDPI_PROTOCOL_MAIL_IMAP, "MAIL_IMAP" },
        { NDPI_PROTOCOL_DNS, "DNS" },
        { NDPI_PROTOCOL_IPP, "IPP" },
        { NDPI_PROTOCOL_HTTP, "HTTP" },
        { NDPI_PROTOCOL_MDNS, "MDNS" },
        { NDPI_PROTOCOL_NTP, "NTP" },
        { NDPI_PROTOCOL_NETBIOS, "NETBIOS" },
        { NDPI_PROTOCOL_NFS, "NFS" },
        { NDPI_PROTOCOL_SSDP, "SSDP" },
        { NDPI_PROTOCOL_BGP, "BGP" },
        { NDPI_PROTOCOL_SNMP, "SNMP" },
        { NDPI_PROTOCOL_XDMCP, "XDMCP" },
        { NDPI_PROTOCOL_SMBV1, "SMBV1" },
        { NDPI_PROTOCOL_SYSLOG, "SYSLOG" },
        { NDPI_PROTOCOL_DHCP, "DHCP" },
        { NDPI_PROTOCOL_POSTGRES, "POSTGRES" },
        { NDPI_PROTOCOL_MYSQL, "MYSQL" },
        { NDPI_PROTOCOL_HOTMAIL, "HOTMAIL" },
        { NDPI_PROTOCOL_DIRECT_DOWNLOAD_LINK, "DIRECT_DOWNLOAD_LINK" },
        { NDPI_PROTOCOL_MAIL_POPS, "MAIL_POPS" },
        { NDPI_PROTOCOL_APPLEJUICE, "APPLEJUICE" },
        { NDPI_PROTOCOL_DIRECTCONNECT, "DIRECTCONNECT" },
        { NDPI_PROTOCOL_NTOP, "NTOP" },
        { NDPI_PROTOCOL_COAP, "COAP" },
        { NDPI_PROTOCOL_VMWARE, "VMWARE" },
        { NDPI_PROTOCOL_MAIL_SMTPS, "MAIL_SMTPS" },
        { NDPI_PROTOCOL_FBZERO, "FBZERO" },
        { NDPI_PROTOCOL_UBNTAC2, "UBNTAC2" },
        { NDPI_PROTOCOL_KONTIKI, "KONTIKI" },
        { NDPI_PROTOCOL_OPENFT, "OPENFT" },
        { NDPI_PROTOCOL_FASTTRACK, "FASTTRACK" },
        { NDPI_PROTOCOL_GNUTELLA, "GNUTELLA" },
        { NDPI_PROTOCOL_EDONKEY, "EDONKEY" },
        { NDPI_PROTOCOL_BITTORRENT, "BITTORRENT" },
        { NDPI_PROTOCOL_SKYPE_CALL, "SKYPE_CALL" },
        { NDPI_PROTOCOL_SIGNAL, "SIGNAL" },
        { NDPI_PROTOCOL_MEMCACHED, "MEMCACHED" },
        { NDPI_PROTOCOL_SMBV23, "SMBV23" },
        { NDPI_PROTOCOL_MINING, "MINING" },
        { NDPI_PROTOCOL_NEST_LOG_SINK, "NEST_LOG_SINK" },
        { NDPI_PROTOCOL_MODBUS, "MODBUS" },
        { NDPI_PROTOCOL_XBOX, "XBOX" },
        { NDPI_PROTOCOL_QQ, "QQ" },
        { NDPI_PROTOCOL_TIKTOK, "TIKTOK" },
        { NDPI_PROTOCOL_RTSP, "RTSP" },
        { NDPI_PROTOCOL_MAIL_IMAPS, "MAIL_IMAPS" },
        { NDPI_PROTOCOL_ICECAST, "ICECAST" },
        { NDPI_PROTOCOL_PPLIVE, "PPLIVE" },
        { NDPI_PROTOCOL_PPSTREAM, "PPSTREAM" },
        { NDPI_PROTOCOL_ZATTOO, "ZATTOO" },
        { NDPI_PROTOCOL_SHOUTCAST, "SHOUTCAST" },
        { NDPI_PROTOCOL_SOPCAST, "SOPCAST" },
        { NDPI_PROTOCOL_TVANTS, "TVANTS" },
        { NDPI_PROTOCOL_TVUPLAYER, "TVUPLAYER" },
        { NDPI_PROTOCOL_HTTP_DOWNLOAD, "HTTP_DOWNLOAD" },
        { NDPI_PROTOCOL_QQLIVE, "QQLIVE" },
        { NDPI_PROTOCOL_THUNDER, "THUNDER" },
        { NDPI_PROTOCOL_SOULSEEK, "SOULSEEK" },
        { NDPI_PROTOCOL_SSL_NO_CERT, "SSL_NO_CERT" },
        { NDPI_PROTOCOL_IRC, "IRC" },
        { NDPI_PROTOCOL_AYIYA, "AYIYA" },
        { NDPI_PROTOCOL_UNENCRYPTED_JABBER, "UNENCRYPTED_JABBER" },
        { NDPI_PROTOCOL_MSN, "MSN" },
        { NDPI_PROTOCOL_OSCAR, "OSCAR" },
        { NDPI_PROTOCOL_YAHOO, "YAHOO" },
        { NDPI_PROTOCOL_BATTLEFIELD, "BATTLEFIELD" },
        { NDPI_PROTOCOL_GOOGLE_PLUS, "GOOGLE_PLUS" },
        { NDPI_PROTOCOL_IP_VRRP, "IP_VRRP" },
        { NDPI_PROTOCOL_STEAM, "STEAM" },
        { NDPI_PROTOCOL_HALFLIFE2, "HALFLIFE2" },
        { NDPI_PROTOCOL_WORLDOFWARCRAFT, "WORLDOFWARCRAFT" },
        { NDPI_PROTOCOL_TELNET, "TELNET" },
        { NDPI_PROTOCOL_STUN, "STUN" },
        { NDPI_PROTOCOL_IP_IPSEC, "IP_IPSEC" },
        { NDPI_PROTOCOL_IP_GRE, "IP_GRE" },
        { NDPI_PROTOCOL_IP_ICMP, "IP_ICMP" },
        { NDPI_PROTOCOL_IP_IGMP, "IP_IGMP" },
        { NDPI_PROTOCOL_IP_EGP, "IP_EGP" },
        { NDPI_PROTOCOL_IP_SCTP, "IP_SCTP" },
        { NDPI_PROTOCOL_IP_OSPF, "IP_OSPF" },
        { NDPI_PROTOCOL_IP_IP_IN_IP, "IP_IP_IN_IP" },
        { NDPI_PROTOCOL_RTP, "RTP" },
        { NDPI_PROTOCOL_RDP, "RDP" },
        { NDPI_PROTOCOL_VNC, "VNC" },
        { NDPI_PROTOCOL_PCANYWHERE, "PCANYWHERE" },
        { NDPI_PROTOCOL_SSL, "SSL" },
        { NDPI_PROTOCOL_SSH, "SSH" },
        { NDPI_PROTOCOL_USENET, "USENET" },
        { NDPI_PROTOCOL_MGCP, "MGCP" },
        { NDPI_PROTOCOL_IAX, "IAX" },
        { NDPI_PROTOCOL_TFTP, "TFTP" },
        { NDPI_PROTOCOL_AFP, "AFP" },
        { NDPI_PROTOCOL_STEALTHNET, "STEALTHNET" },
        { NDPI_PROTOCOL_AIMINI, "AIMINI" },
        { NDPI_PROTOCOL_SIP, "SIP" },
        { NDPI_PROTOCOL_TRUPHONE, "TRUPHONE" },
        { NDPI_PROTOCOL_IP_ICMPV6, "IP_ICMPV6" },
        { NDPI_PROTOCOL_DHCPV6, "DHCPV6" },
        { NDPI_PROTOCOL_ARMAGETRON, "ARMAGETRON" },
        { NDPI_PROTOCOL_CROSSFIRE, "CROSSFIRE" },
        { NDPI_PROTOCOL_DOFUS, "DOFUS" },
        { NDPI_PROTOCOL_FIESTA, "FIESTA" },
        { NDPI_PROTOCOL_FLORENSIA, "FLORENSIA" },
        { NDPI_PROTOCOL_GUILDWARS, "GUILDWARS" },
        { NDPI_PROTOCOL_HTTP_ACTIVESYNC, "HTTP_ACTIVESYNC" },
        { NDPI_PROTOCOL_KERBEROS, "KERBEROS" },
        { NDPI_PROTOCOL_LDAP, "LDAP" },
        { NDPI_PROTOCOL_MAPLESTORY, "MAPLESTORY" },
        { NDPI_PROTOCOL_MSSQL_TDS, "MSSQL_TDS" },
        { NDPI_PROTOCOL_PPTP, "PPTP" },
        { NDPI_PROTOCOL_WARCRAFT3, "WARCRAFT3" },
        { NDPI_PROTOCOL_WORLD_OF_KUNG_FU, "WORLD_OF_KUNG_FU" },
        { NDPI_PROTOCOL_SLACK, "SLACK" },
        { NDPI_PROTOCOL_FACEBOOK, "FACEBOOK" },
        { NDPI_PROTOCOL_TWITTER, "TWITTER" },
        { NDPI_PROTOCOL_DROPBOX, "DROPBOX" },
        { NDPI_PROTOCOL_GMAIL, "GMAIL" },
        { NDPI_PROTOCOL_GOOGLE_MAPS, "GOOGLE_MAPS" },
        { NDPI_PROTOCOL_YOUTUBE, "YOUTUBE" },
        { NDPI_PROTOCOL_SKYPE, "SKYPE" },
        { NDPI_PROTOCOL_GOOGLE, "GOOGLE" },
        { NDPI_PROTOCOL_DCERPC, "DCERPC" },
        { NDPI_PROTOCOL_NETFLOW, "NETFLOW" },
        { NDPI_PROTOCOL_SFLOW, "SFLOW" },
        { NDPI_PROTOCOL_HTTP_CONNECT, "HTTP_CONNECT" },
        { NDPI_PROTOCOL_HTTP_PROXY, "HTTP_PROXY" },
        { NDPI_PROTOCOL_CITRIX, "CITRIX" },
        { NDPI_PROTOCOL_NETFLIX, "NETFLIX" },
        { NDPI_PROTOCOL_LASTFM, "LASTFM" },
        { NDPI_PROTOCOL_WAZE, "WAZE" },
        { NDPI_PROTOCOL_YOUTUBE_UPLOAD, "YOUTUBE_UPLOAD" },
        { NDPI_PROTOCOL_GENERIC, "GENERIC" },
        { NDPI_PROTOCOL_CHECKMK, "CHECKMK" },
        { NDPI_PROTOCOL_AJP, "AJP" },
        { NDPI_PROTOCOL_APPLE, "APPLE" },
        { NDPI_PROTOCOL_WEBEX, "WEBEX" },
        { NDPI_PROTOCOL_WHATSAPP, "WHATSAPP" },
        { NDPI_PROTOCOL_APPLE_ICLOUD, "APPLE_ICLOUD" },
        { NDPI_PROTOCOL_VIBER, "VIBER" },
        { NDPI_PROTOCOL_APPLE_ITUNES, "APPLE_ITUNES" },
        { NDPI_PROTOCOL_RADIUS, "RADIUS" },
        { NDPI_PROTOCOL_WINDOWS_UPDATE, "WINDOWS_UPDATE" },
        { NDPI_PROTOCOL_TEAMVIEWER, "TEAMVIEWER" },
        { NDPI_PROTOCOL_TUENTI, "TUENTI" },
        { NDPI_PROTOCOL_LOTUS_NOTES, "LOTUS_NOTES" },
        { NDPI_PROTOCOL_SAP, "SAP" },
        { NDPI_PROTOCOL_GTP, "GTP" },
        { NDPI_PROTOCOL_UPNP, "UPNP" },
        { NDPI_PROTOCOL_LLMNR, "LLMNR" },
        { NDPI_PROTOCOL_REMOTE_SCAN, "REMOTE_SCAN" },
        { NDPI_PROTOCOL_SPOTIFY, "SPOTIFY" },
        { NDPI_PROTOCOL_MESSENGER, "MESSENGER" },
        { NDPI_PROTOCOL_H323, "H323" },
        { NDPI_PROTOCOL_OPENVPN, "OPENVPN" },
        { NDPI_PROTOCOL_NOE, "NOE" },
        { NDPI_PROTOCOL_CISCOVPN, "CISCOVPN" },
        { NDPI_PROTOCOL_TEAMSPEAK, "TEAMSPEAK" },
        { NDPI_PROTOCOL_TOR, "TOR" },
        { NDPI_PROTOCOL_SKINNY, "SKINNY" },
        { NDPI_PROTOCOL_RTCP, "RTCP" },
        { NDPI_PROTOCOL_RSYNC, "RSYNC" },
        { NDPI_PROTOCOL_ORACLE, "ORACLE" },
        { NDPI_PROTOCOL_CORBA, "CORBA" },
        { NDPI_PROTOCOL_UBUNTUONE, "UBUNTUONE" },
        { NDPI_PROTOCOL_WHOIS_DAS, "WHOIS_DAS" },
        { NDPI_PROTOCOL_COLLECTD, "COLLECTD" },
        { NDPI_PROTOCOL_SOCKS, "SOCKS" },
        { NDPI_PROTOCOL_NINTENDO, "NINTENDO" },
        { NDPI_PROTOCOL_RTMP, "RTMP" },
        { NDPI_PROTOCOL_FTP_DATA, "FTP_DATA" },
        { NDPI_PROTOCOL_WIKIPEDIA, "WIKIPEDIA" },
        { NDPI_PROTOCOL_ZMQ, "ZMQ" },
        { NDPI_PROTOCOL_AMAZON, "AMAZON" },
        { NDPI_PROTOCOL_EBAY, "EBAY" },
        { NDPI_PROTOCOL_CNN, "CNN" },
        { NDPI_PROTOCOL_MEGACO, "MEGACO" },
        { NDPI_PROTOCOL_REDIS, "REDIS" },
        { NDPI_PROTOCOL_PANDO, "PANDO" },
        { NDPI_PROTOCOL_VHUA, "VHUA" },
        { NDPI_PROTOCOL_TELEGRAM, "TELEGRAM" },
        { NDPI_PROTOCOL_VEVO, "VEVO" },
        { NDPI_PROTOCOL_PANDORA, "PANDORA" },
        { NDPI_PROTOCOL_QUIC, "QUIC" },
        { NDPI_PROTOCOL_WHATSAPP_VOICE, "WHATSAPP_VOICE" },
        { NDPI_PROTOCOL_EAQ, "EAQ" },
        { NDPI_PROTOCOL_OOKLA, "OOKLA" },
        { NDPI_PROTOCOL_AMQP, "AMQP" },
        { NDPI_PROTOCOL_KAKAOTALK, "KAKAOTALK" },
        { NDPI_PROTOCOL_KAKAOTALK_VOICE, "KAKAOTALK_VOICE" },
        { NDPI_PROTOCOL_TWITCH, "TWITCH" },
        { NDPI_PROTOCOL_FREE_196, "FREE_196" },
        { NDPI_PROTOCOL_WECHAT, "WECHAT" },
        { NDPI_PROTOCOL_MPEGTS, "MPEGTS" },
        { NDPI_PROTOCOL_SNAPCHAT, "SNAPCHAT" },
        { NDPI_PROTOCOL_SINA, "SINA" },
        { NDPI_PROTOCOL_HANGOUT, "HANGOUT" },
        { NDPI_PROTOCOL_IFLIX, "IFLIX" },
        { NDPI_PROTOCOL_GITHUB, "GITHUB" },
        { NDPI_PROTOCOL_BJNP, "BJNP" },
        { NDPI_PROTOCOL_FREE_205, "FREE_205" },
        { NDPI_PROTOCOL_FREE_206, "FREE_206" },
        { NDPI_PROTOCOL_SMPP, "SMPP" },
        { NDPI_PROTOCOL_DNSCRYPT, "DNSCRYPT" },
        { NDPI_PROTOCOL_TINC, "TINC" },
        { NDPI_PROTOCOL_DEEZER, "DEEZER" },
        { NDPI_PROTOCOL_INSTAGRAM, "INSTAGRAM" },
        { NDPI_PROTOCOL_MICROSOFT, "MICROSOFT" },
        { NDPI_PROTOCOL_STARCRAFT, "STARCRAFT" },
        { NDPI_PROTOCOL_TEREDO, "TEREDO" },
        { NDPI_PROTOCOL_HOTSPOT_SHIELD, "HOTSPOT_SHIELD" },
        { NDPI_PROTOCOL_HEP, "HEP" },
        { NDPI_PROTOCOL_GOOGLE_DRIVE, "GOOGLE_DRIVE" },
        { NDPI_PROTOCOL_OCS, "OCS" },
        { NDPI_PROTOCOL_OFFICE_365, "OFFICE_365" },
        { NDPI_PROTOCOL_CLOUDFLARE, "CLOUDFLARE" },
        { NDPI_PROTOCOL_MS_ONE_DRIVE, "MS_ONE_DRIVE" },
        { NDPI_PROTOCOL_MQTT, "MQTT" },
        { NDPI_PROTOCOL_RX, "RX" },
        { NDPI_PROTOCOL_APPLESTORE, "APPLESTORE" },
        { NDPI_PROTOCOL_OPENDNS, "OPENDNS" },
        { NDPI_PROTOCOL_GIT, "GIT" },
        { NDPI_PROTOCOL_DRDA, "DRDA" },
        { NDPI_PROTOCOL_PLAYSTORE, "PLAYSTORE" },
        { NDPI_PROTOCOL_SOMEIP, "SOMEIP" },
        { NDPI_PROTOCOL_FIX, "FIX" },
        { NDPI_PROTOCOL_PLAYSTATION, "PLAYSTATION" },
        { NDPI_PROTOCOL_PASTEBIN, "PASTEBIN" },
        { NDPI_PROTOCOL_LINKEDIN, "LINKEDIN" },
        { NDPI_PROTOCOL_SOUNDCLOUD, "SOUNDCLOUD" },
        { NDPI_PROTOCOL_CSGO, "CSGO" },
        { NDPI_PROTOCOL_LISP, "LISP" },
        { NDPI_PROTOCOL_DIAMETER, "DIAMETER" },
        { NDPI_PROTOCOL_APPLE_PUSH, "APPLE_PUSH" },
        { NDPI_PROTOCOL_GOOGLE_SERVICES, "GOOGLE_SERVICES" },
        { NDPI_PROTOCOL_AMAZON_VIDEO, "AMAZON_VIDEO" },
        { NDPI_PROTOCOL_GOOGLE_DOCS, "GOOGLE_DOCS" },
        { NDPI_PROTOCOL_WHATSAPP_FILES, "WHATSAPP_FILES" },
};

static std::map<int, std::string> category_name_map = {
        { NDPI_PROTOCOL_CATEGORY_UNSPECIFIED, "UNSPECIFIED" },
        { NDPI_PROTOCOL_CATEGORY_MEDIA, "MEDIA" },
        { NDPI_PROTOCOL_CATEGORY_VPN, "VPN" },
        { NDPI_PROTOCOL_CATEGORY_MAIL, "MAIL" },
        { NDPI_PROTOCOL_CATEGORY_DATA_TRANSFER, "DATA_TRANSFER" },
        { NDPI_PROTOCOL_CATEGORY_WEB, "WEB" },
        { NDPI_PROTOCOL_CATEGORY_SOCIAL_NETWORK, "SOCIAL_NETWORK" },
        { NDPI_PROTOCOL_CATEGORY_DOWNLOAD_FT, "DOWNLOAD_FT" },
        { NDPI_PROTOCOL_CATEGORY_GAME, "GAME" },
        { NDPI_PROTOCOL_CATEGORY_CHAT, "CHAT" },
        { NDPI_PROTOCOL_CATEGORY_VOIP, "VOIP" },
        { NDPI_PROTOCOL_CATEGORY_DATABASE, "DATABASE" },
        { NDPI_PROTOCOL_CATEGORY_REMOTE_ACCESS, "REMOTE_ACCESS" },
        { NDPI_PROTOCOL_CATEGORY_CLOUD, "CLOUD" },
        { NDPI_PROTOCOL_CATEGORY_NETWORK, "NETWORK" },
        { NDPI_PROTOCOL_CATEGORY_COLLABORATIVE, "COLLABORATIVE" },
        { NDPI_PROTOCOL_CATEGORY_RPC, "RPC" },
        { NDPI_PROTOCOL_CATEGORY_STREAMING, "STREAMING" },
        { NDPI_PROTOCOL_CATEGORY_SYSTEM_OS, "SYSTEM_OS" },
        { NDPI_PROTOCOL_CATEGORY_SW_UPDATE, "SW_UPDATE" },
        { NDPI_PROTOCOL_CATEGORY_MUSIC, "MUSIC" },
        { NDPI_PROTOCOL_CATEGORY_VIDEO, "VIDEO" },
        { NDPI_PROTOCOL_CATEGORY_SHOPPING, "SHOPPING" },
        { NDPI_PROTOCOL_CATEGORY_PRODUCTIVITY, "PRODUCTIVITY" },
        { NDPI_PROTOCOL_CATEGORY_FILE_SHARING, "FILE_SHARING" },
        { CUSTOM_CATEGORY_MINING, "MINING" },
        { CUSTOM_CATEGORY_MALWARE, "MALWARE" },
        { CUSTOM_CATEGORY_ADVERTISEMENT, "ADVERTISEMENT" },
        { CUSTOM_CATEGORY_BANNED_SITE, "BANNED_SITE" },
        { CUSTOM_CATEGORY_SITE_UNAVAILABLE, "SITE_UNAVAILABLE" }
};

void process_detection(pcpp::RawPacket* packet, const cookie_t* cookie) {
    pcpp::Packet parsedPacket{ packet };

    uint32_t src_ip{}, dst_ip{};
    uint16_t src_port{}, dst_port{};
    uint8_t proto{};

    if (packet->getLinkLayerType() != pcpp::LinkLayerType::LINKTYPE_ETHERNET)
        return;

    ndpi_iphdr* iphdr{};
    size_t data_len{};

    setup_parameters(parsedPacket, proto, src_ip, dst_ip, src_port, dst_port, iphdr, data_len);

    auto& workflow = cookie->workflow;

    flow_cmp_t flow_compare = [&](const flow_t_ptr& f) -> bool {
        return f->src_ip == src_ip && f->dst_ip == dst_ip
               && f->src_port == src_port && f->dst_port == dst_port
               && f->proto == proto;
    };

    flow_cmp_t flow_compare_reverse = [&](const flow_t_ptr& f) -> bool {
        return f->src_ip == dst_ip && f->dst_ip == src_ip
               && f->src_port == dst_port && f->dst_port == src_port
               && f->proto == proto;
    };

    flow_t* flow = get_flow(workflow->ndpi_flows_root, flow_compare, flow_compare_reverse);

    if (flow == nullptr) {
        flow = workflow->ndpi_flows_root.emplace_back(std::make_shared<flow_t>()).get();
        flow->src_ip = src_ip;
        flow->dst_ip = dst_ip;
        flow->src_port = src_port;
        flow->dst_port = dst_port;
        flow->proto = proto;

        flow->src2dst_packets++;
        flow->id = workflow->last_id++;
    }

    DPI::Packet p{
        .flow_id = flow->id,
        .direction = flow->src_to_dst_direction ? DPI::Direction::Forward : DPI::Direction::Reverse,
        .time_stamp = packet->getPacketTimeStamp(),
        .frame_length = static_cast<uint32_t>(packet->getRawDataLen())
    };

    workflow->dump.packets.emplace_back(p);

    auto detection_module = cookie->detection_module;

    ndpi_id_struct *src_id{}, *dst_id{};

    if (flow->src_to_dst_direction) {
        src_id = flow->src_id.get();
        dst_id = flow->dst_id.get();
    } else {
        src_id = flow->dst_id.get();
        dst_id = flow->src_id.get();
    }

    uint64_t time = packet->getPacketTimeStamp().tv_sec;

    std::lock_guard<std::mutex> lock(flow->flow_mutex);

    if (flow->fully_completed)
        return;

    if (flow->detection_completed) {

        if (flow->check_extra_packets && flow->flow->check_extra_packets) {

            if (flow->flow->num_extra_packets_checked == 0 && flow->flow->max_extra_packets_to_check == 0) {
                flow->flow->max_extra_packets_to_check = MAX_EXTRA_PACKETS_TO_CHECK;
            }

            if (flow->flow->num_extra_packets_checked < flow->flow->max_extra_packets_to_check) {
                ndpi_process_extra_packet(
                        detection_module.get(), flow->flow.get(), (uint8_t*)iphdr, data_len, time, src_id, dst_id);
                if (flow->flow->check_extra_packets == 0) {
                    flow->check_extra_packets = false;
                    // protocol is detected

                    std::wcout
                            << L"Detected protocol: "
                            << strings::to_wide(get_protocol_name(flow->detected_protocol.app_protocol))
                            << L" Detected category: "
                            << strings::to_wide(get_category_name(flow->detected_protocol))
                            << std::endl;
                    LOG
                        << "Flow: " << pcpp::IPv4Address(flow->src_ip).toString() << ":" << flow->src_port
                        << "<->" << pcpp::IPv4Address(flow->dst_ip).toString() << ":" << flow->dst_port
                        << " Detected protocol: "
                        << get_protocol_name(flow->detected_protocol.app_protocol)
                        << " Category: "
                        << get_category_name(flow->detected_protocol);

                    auto flow_it = std::find_if(workflow->dump.flows.begin(), workflow->dump.flows.end(),
                                                [&p](const DPI::Flow& f){ return p.flow_id == f.id; });

                    if (flow_it != workflow->dump.flows.end()) {
                        (*flow_it).master_protocol = get_protocol_name(flow->detected_protocol.master_protocol);
                        (*flow_it).app_protocol = get_protocol_name(flow->detected_protocol.app_protocol);
                        (*flow_it).category = get_category_name(flow->detected_protocol);
                        (*flow_it).bidirectional = flow->bidirectional;
                    }

                    flow->printed = true;

                    return;
                }
            }
        } else {
            if (!flow->printed) {
                std::wcout
                        << L"Detected protocol: "
                        << strings::to_wide(get_protocol_name(flow->detected_protocol.app_protocol))
                        << L" Detected category: "
                        << strings::to_wide(get_category_name(flow->detected_protocol))
                        << std::endl;

                LOG
                        << "Flow: " << pcpp::IPv4Address(flow->src_ip).toString() << ":" << flow->src_port
                        << "<->" << pcpp::IPv4Address(flow->dst_ip).toString() << ":" << flow->dst_port
                        << " Detected protocol: "
                        << get_protocol_name(flow->detected_protocol.app_protocol)
                        << " Category: "
                        << get_category_name(flow->detected_protocol);


                auto flow_it = std::find_if(workflow->dump.flows.begin(), workflow->dump.flows.end(),
                        [&p](const DPI::Flow& f){ return p.flow_id == f.id; });

                if (flow_it != workflow->dump.flows.end()) {
                    (*flow_it).master_protocol = get_protocol_name(flow->detected_protocol.master_protocol);
                    (*flow_it).app_protocol = get_protocol_name(flow->detected_protocol.app_protocol);
                    (*flow_it).category = get_category_name(flow->detected_protocol);
                    (*flow_it).bidirectional = flow->bidirectional;
                }
            }

            free_flow(*flow);
            flow->fully_completed = true;
        }
    }

    flow->detected_protocol = ndpi_detection_process_packet(
            detection_module.get(),
            flow->flow.get(),
            (uint8_t*)iphdr,
            data_len,
            time,
            src_id,
            dst_id
    );

    DPI::Flow f{
            .id = flow->id,
            .src_ip = flow->src_ip,
            .dst_ip = flow->dst_ip,
            .src_port = flow->src_port,
            .dst_port = flow->dst_port,
            .master_protocol = get_protocol_name(flow->detected_protocol.master_protocol),
            .app_protocol = get_protocol_name(flow->detected_protocol.app_protocol),
            .category = get_category_name(flow->detected_protocol),
            .bidirectional = flow->bidirectional
    };

    workflow->dump.flows.emplace_back(f);

    if ((flow->detected_protocol.app_protocol != NDPI_PROTOCOL_UNKNOWN)
        || ((proto == IPPROTO_UDP) && ((flow->src2dst_packets + flow->dst2src_packets) > 8))
        || ((proto == IPPROTO_TCP) && ((flow->src2dst_packets + flow->dst2src_packets) > 10))
            ) {
        flow->detection_completed = true;
        flow->check_extra_packets = true;

        if (flow->detected_protocol.app_protocol == NDPI_PROTOCOL_UNKNOWN) {
            flow->detected_protocol = ndpi_detection_giveup(detection_module.get(), flow->flow.get(), 1);
        }
    }
}

std::string get_protocol_name(const uint16_t& proto) {
    return protocol_name_map.at(proto);
}

void setup_parameters(
        pcpp::Packet &packet, uint8_t& proto,
        uint32_t &src_ip, uint32_t &dst_ip, uint16_t &src_port, uint16_t &dst_port,
        ndpi_iphdr*& iphdr, size_t& data_len
) {
    const auto ipv4Layer = packet.getLayerOfType<pcpp::IPv4Layer>();
    if (ipv4Layer) {

        src_ip = ipv4Layer->getSrcIpAddress().toInt();
        dst_ip = ipv4Layer->getDstIpAddress().toInt();

        if (const auto tcpLayer = packet.getLayerOfType<pcpp::TcpLayer>()) {
            src_port = ntohs(tcpLayer->getTcpHeader()->portSrc);
            dst_port = ntohs(tcpLayer->getTcpHeader()->portDst);
            proto = IPPROTO_TCP;
        } else if (const auto udpLayer = packet.getLayerOfType<pcpp::UdpLayer>()) {
            src_port = ntohs(udpLayer->getUdpHeader()->portSrc);
            dst_port = ntohs(udpLayer->getUdpHeader()->portDst);
            proto = IPPROTO_UDP;
        } else {
            return;
        }

        iphdr = reinterpret_cast<ndpi_iphdr *>(ipv4Layer->getIPv4Header());
        data_len = ipv4Layer->getDataLen();
    }
}

/*
 * return value can be nullptr if needed flow is currently not present in flows_root
 */
flow_t* get_flow(std::vector<flow_t_ptr>& flows_root, flow_cmp_t& comparator, flow_cmp_t& reverse_comparator) {
    std::vector<flow_t_ptr>::iterator flow_it;
    flow_t* flow{};

    flow_it = std::find_if(flows_root.begin(), flows_root.end(), comparator);
    if (flow_it == flows_root.cend()) {
        flow_it = std::find_if(flows_root.begin(), flows_root.end(), reverse_comparator);
    } else {
        flow = (*flow_it).get();
        flow->src_to_dst_direction = 1;

        flow->src2dst_packets++;
    }

    if (flow_it != flows_root.end()) {
        flow = (*flow_it).get();
        flow->bidirectional = 1;
        flow->src_to_dst_direction = 0;

        flow->dst2src_packets++;
    }

    return flow;
}

void free_flow(flow_t& flow) {
    if (flow.flow) {
//        ndpi_free(flow.flow.get());
        flow.flow.reset();
    }

    if (flow.src_ip) {
        flow.src_id.reset();
    }

    if (flow.dst_id) {
        flow.dst_id.reset();
    }

    std::wcout << L"Free flow with id " << flow.id << std::endl;
}

std::string get_category_name(const ndpi_protocol& proto) {
    auto it = category_name_map.find(proto.category);
    if (it != category_name_map.cend()) {
        return it->second;
    } else {
        return "Unknown";
    }
}