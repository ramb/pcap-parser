cmake_minimum_required(VERSION 3.12)
project(ndpi_example)

set(CMAKE_C_STANDARD 11)
#set(CMAKE_CXX_STANDARD 17)
include(../FindPCAP.cmake)

find_package(PCAP REQUIRED)
#find_package(Boost REQUIRED)

add_executable(ndpi_example
        main.c
        ndpi_util.c
        uthash.h
    )

#add_definitions(-static-libstdc++)

target_link_libraries(ndpi_example
        ${PCAP_LIBRARY}
        pthread
        ndpi
        )

add_custom_command(
        TARGET ndpi_example
        POST_BUILD
        COMMAND bash -c "sudo setcap cap_net_raw,cap_net_admin=eip /home/ramb/Projects/pcap_extract/cmake-build-debug/ndpi_example/ndpi_example"
)