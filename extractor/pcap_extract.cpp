#include "persist/stdafx.h"

#include <pcapplusplus/PcapFileDevice.h>
#include <pcapplusplus/RawPacket.h>
#include <pcapplusplus/Packet.h>
#include <pcapplusplus/TcpLayer.h>
#include <pcapplusplus/HttpLayer.h>
#include <pcapplusplus/IPv4Layer.h>

#include <boost/algorithm/string.hpp>
#include <boost/format.hpp>
#include <boost/program_options.hpp>

#include "persist/logger.h"
#include "persist/csvwriter.h"
#include "persist/file_path.h"
#include "persist/core.h"

#include "config.h"


int main(int argc, char** argv)
{
    LOG << "init";
    try
    {
        LOG << "start read config";
        Config cfg{ argc, argv };
        LOG << "config is read";

        std::unique_ptr<pcpp::IFileReaderDevice> reader;
        reader.reset(pcpp::PcapNgFileReaderDevice::getReader(cfg.input_path));

        if (!reader)
        {
            throw std::runtime_error("Cannot determine reader for file type");
        }

        if (!reader->open())
        {
            throw std::runtime_error("File not opened");
        }

        if (!reader->setFilter("tcp"))
        {
            throw std::runtime_error("Cannot set filter");
        }


        CSVWriter csvWriter{};
        csvWriter.newRow()
                << "Source" << "Host" << "Method" << "URI" << "Timestamp"
                << "User-Agent" << "Cookie" << "Content-Type" << "Body";
        pcpp::RawPacket rawPacket;

        while (reader->getNextPacket(rawPacket))
        {
            pcpp::Packet parsedPacket{ &rawPacket };
            auto httpRequestLayer = parsedPacket.getLayerOfType<pcpp::HttpRequestLayer>();
            auto httpResponseLayer = parsedPacket.getLayerOfType<pcpp::HttpResponseLayer>();
            if (httpRequestLayer)
            {
                auto getFieldValue = [&httpRequestLayer](const std::string& field) -> std::string {
                    auto ptr = httpRequestLayer->getFieldByName(field);
                    if (ptr != nullptr)
                        return ptr->getFieldValue();
                    return std::string{};
                };

                const auto tcpLayer = parsedPacket.getLayerOfType<pcpp::IPv4Layer>();
                const auto source = tcpLayer->getSrcIpAddress().toString();
                const auto host = getFieldValue(PCPP_HTTP_HOST_FIELD);
                const auto method = core::printHttpMethod(httpRequestLayer->getFirstLine()->getMethod());
                const auto uri = httpRequestLayer->getFirstLine()->getUri();
                const auto userAgent = getFieldValue(PCPP_HTTP_USER_AGENT_FIELD);
                const auto cookie = getFieldValue(PCPP_HTTP_COOKIE_FIELD);

                long int timestamp = rawPacket.getPacketTimeStamp().tv_sec;

                csvWriter.newRow()
                        << source << host << method << long_string{ uri } << timestamp
                        << long_string{ userAgent } << long_string{ cookie };

                if (method == "POST") {
                    const auto contentType = getFieldValue(PCPP_HTTP_CONTENT_TYPE_FIELD);

                    const auto dataPtr = httpRequestLayer->getLayerPayload();
                    const auto dataLen = httpRequestLayer->getLayerPayloadSize();
                    const std::string data{ dataPtr, dataPtr + dataLen };
                    csvWriter << contentType << long_string{ data };
                } else {
                    csvWriter << "" << "";
                }
            } else if (httpResponseLayer) {

            }
        }

        csvWriter.writeToFile(cfg.output_path);

        return 0;
    } catch (const std::exception& e) {
        std::cout << "Internal error occurred: " << e.what() << std::endl;
        return 1;
    }
}
