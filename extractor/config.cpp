#include "config.h"

#include "persist/stdafx.h"
#include <boost/program_options.hpp>

namespace po = boost::program_options;

Config::Config(int argc, char** argv)
{
    po::options_description desc("Allowed options");
    desc.add_options()
            ("help", "produce help message")
            ("input", po::value<std::string>(), "path to input pcap(ng) file")
            ("output", po::value<std::string>(), "path to output csv file")
            ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
        std::cout << desc << std::endl;
        exit(0);
    }

    if (vm.count("input")) {
        input_path = { vm.at("input").as<std::string>() };
        std::wcout << L"input path: " << strings::to_wide(input_path) << std::endl;
    } else {
        throw std::runtime_error("input file path should not be empty");
    }

    if (vm.count("output")) {
        output_path = { vm.at("output").as<std::string>() };
        std::wcout << L"output path: " << strings::to_wide(output_path) << std::endl;
    } else {
        throw std::runtime_error("output file path should not be empty");
    }
}