#pragma once

#include "persist/file_path.h"

class Config {
public:
    Config(int argc, char** argv);
    file_path input_path;
    file_path output_path;
};