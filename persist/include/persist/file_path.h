#pragma once

#include <iostream>
#include <filesystem>
#include <boost/archive/basic_archive.hpp>
#include <boost/serialization/version.hpp>

#include "stdafx.h"

namespace fs = std::filesystem;

class file_path
{
    friend class boost::serialization::access;
    template <typename Archive> void save(Archive& ar, const unsigned version) const {
        std::string path = path_.string();
        ar & BOOST_SERIALIZATION_NVP(path);
    }

    template <typename Archive> void load(Archive& ar, const unsigned version) {
        std::string path;
        ar & BOOST_SERIALIZATION_NVP(path);
        path_ = fs::absolute(path);
    }

    BOOST_SERIALIZATION_SPLIT_MEMBER()

public:
    file_path() = default;
    file_path(const std::string& path);
    file_path(const std::wstring& wpath);
    ~file_path() = default;

    operator const char *() const;
    operator const std::string() const;
    operator const wchar_t *() const;
    operator const std::wstring() const;
private:
    fs::path path_;
};

BOOST_CLASS_VERSION(file_path, 0)