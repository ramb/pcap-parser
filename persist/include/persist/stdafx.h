#pragma once

#include <iostream>
#include <cstdio>
#include <vector>
#include <memory>
#include <exception>
#include <stdexcept>
#include <fstream>
#include <thread>
#include <chrono>
#include <functional>
#include <atomic>
#include <condition_variable>
#include <mutex>
#include <codecvt>

#include <pcap.h>
#include <boost/serialization/split_free.hpp>
#include <boost/serialization/nvp.hpp>
#include <boost/serialization/version.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/unique_ptr.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <uuid.h>

#include "logger.h"

namespace strings
{

static std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;

inline std::wstring to_wide(const std::string& str) {
    return converter.from_bytes(str);
}

inline std::string to_narrow(const std::wstring& wstr) {
    return converter.to_bytes(wstr);
}

}

BOOST_SERIALIZATION_SPLIT_FREE(uuids::uuid)
BOOST_SERIALIZATION_SPLIT_FREE(std::chrono::system_clock::time_point)

namespace boost {
namespace serialization {

    template <typename Archive>
    void save(Archive& ar, const uuids::uuid& uuid, const unsigned int version) {
        std::string tmp = uuids::to_string(uuid);
        ar & BOOST_SERIALIZATION_NVP(tmp);
    }

    template <typename Archive>
    void load(Archive& ar, uuids::uuid& uuid, const unsigned int version) {
        std::string tmp;
        ar & BOOST_SERIALIZATION_NVP(tmp);
        uuid = uuids::uuid::from_string(tmp).value();
    }

    template <typename Archive>
    void save(Archive& ar, const std::chrono::system_clock::time_point& tp, const unsigned version) {
        std::chrono::milliseconds::rep mls = std::chrono::duration_cast<std::chrono::milliseconds>(tp.time_since_epoch()).count();
        ar & BOOST_SERIALIZATION_NVP(mls);
    }

    template <typename Archive>
    void load(Archive& ar, std::chrono::system_clock::time_point& tp, const unsigned version) {
        std::chrono::milliseconds::rep mls;
        ar & BOOST_SERIALIZATION_NVP(mls);
        tp = std::chrono::time_point<std::chrono::system_clock>(std::chrono::milliseconds(mls));
    }

}
}

