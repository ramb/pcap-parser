#pragma once

#include <iostream>
#include <fstream>
#include <ctime>
#include <iomanip>

class Logger
{
public:
    static Logger& instance() {
        static Logger instance_;
        return instance_;
    }

    Logger& nextRow() {
        std::time_t time = std::time(nullptr);
        stream_ << std::endl << std::put_time(std::gmtime(&time), "[%c %Z] ");
        return *this;
    }

    Logger(const Logger&) = delete;
    Logger& operator=(const Logger&) = delete;

    template <typename T>
    Logger& operator << (const T& data) {
        stream_ << data;
        return *this;
    }

private:
    Logger() {
        std::time_t time = std::time(nullptr);
        stream_.open("pcap.log");
        stream_ << std::put_time(std::gmtime(&time), "[%c %Z] ") << "Log file has been created";
    };

    ~Logger() {
        stream_ << std::endl;
        stream_.close();
    }

private:
    std::ofstream stream_;
};

#define LOG Logger::instance().nextRow()
