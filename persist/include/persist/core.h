#pragma once

#include <pcapplusplus/HttpLayer.h>

namespace core
{
    void printInterfaces();
    std::string printHttpMethod(const pcpp::HttpRequestLayer::HttpMethod& httpMethod);
}
