#include "core.h"

#include <pcapplusplus/PcapLiveDeviceList.h>

#include "stdafx.h"
#include "logger.h"

void core::printInterfaces() {
    const auto& devices = pcpp::PcapLiveDeviceList::getInstance().getPcapLiveDevicesList();
    LOG << "List of available devices: ";
    std::wcout << "List of available devices: " << std::endl;
    for (const auto& dev : devices) {
        if (dev->getLoopback() || dev->getIPv4Address().toString() == "0.0.0.0") {
            continue;
        }

        LOG
                << "name: " << dev->getName()
                << ", ip: " << dev->getIPv4Address().toString();

        std::wcout
                << L"name: " << dev->getName()
                << L", ip: " << dev->getIPv4Address().toString().c_str()
                << std::endl;
    }
}

std::string core::printHttpMethod(const pcpp::HttpRequestLayer::HttpMethod& httpMethod)
{
    switch (httpMethod)
    {
        case pcpp::HttpRequestLayer::HttpGET:
            return "GET";
        case pcpp::HttpRequestLayer::HttpPOST:
            return "POST";
        case pcpp::HttpRequestLayer::HttpPUT:
            return "PUT";
        case pcpp::HttpRequestLayer::HttpCONNECT:
            return "CONNECT";
        case pcpp::HttpRequestLayer::HttpDELETE:
            return "DELETE";
        case pcpp::HttpRequestLayer::HttpPATCH:
            return "PATCH";
        default:
            return "Other";
    }
}
