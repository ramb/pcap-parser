#include "file_path.h"

#include <cstdlib>
#include <boost/algorithm/string.hpp>

file_path::file_path(const std::string &path)
{
    if (!path.empty() && path.at(0) == '~')
    {
        const std::string home_path = fs::path(std::getenv("HOME")).string();
        path_ = fs::path(boost::algorithm::replace_first_copy(path, "~", home_path));
        return;
    }
    path_ = fs::absolute(path);
}

file_path::file_path(const std::wstring &wpath)
    : file_path(strings::to_narrow(wpath))
{}

file_path::operator const std::string() const {
    return path_.string();
}

file_path::operator const char *() const {
    return path_.c_str();
}

file_path::operator const std::wstring() const {
    return strings::to_wide(path_.string());
}

file_path::operator const wchar_t *() const {
    return strings::to_wide(path_.string()).c_str();
}
